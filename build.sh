#/bin/bash

docker login -u $GITLAB_CI_USER -p $GITLAB_CI_TOKEN registry.gitlab.com

docker build --pull -t registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp .
docker push registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp

docker build --pull -f Dockerfile.helm -t registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:helm .
docker push registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:helm

docker build --pull -f Dockerfile.octane -t registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:octane .
docker push registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:octane

docker build --pull -f Dockerfile.dev -t registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:dev .
docker push registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:dev

docker build --pull -f Dockerfile.testing -t registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:testing .
docker push registry.gitlab.com/multitech-osp/dockerfiles/php/8.3-frankenphp:testing