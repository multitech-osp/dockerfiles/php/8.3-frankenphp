FROM dunglas/frankenphp:latest-php8.3-alpine

ENV ACCEPT_EULA=Y

RUN apk add \
    supervisor 

# add additional extensions here:
RUN install-php-extensions \
    pdo_mysql \
    gd \
    intl \
    zip \
    opcache \
    sqlsrv \
    pdo_sqlsrv \
    ldap \
    redis \
    pcntl \
    opcache \
    curl \
    filter \
    iconv \
    mbstring \
    exif \
    bcmath \
    tokenizer

# Install datadog tracer
RUN curl -LO https://github.com/DataDog/dd-trace-php/releases/latest/download/datadog-setup.php
RUN php datadog-setup.php --php-bin all --enable-appsec

# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER=1

COPY ./docker/crontab /etc/crontab
RUN chmod 0644 /etc/crontab && crontab /etc/crontab

COPY ./docker/openssl.cnf /etc/ssl/openssl.cnf
COPY ./docker/Caddyfile /etc/caddy/Caddyfile

COPY ./docker/supervisord.conf /etc/supervisor/supervisord.conf
COPY ./docker/php.ini $PHP_INI_DIR/php.ini
COPY ./docker/init /usr/local/bin/docker-php-entrypoint

RUN sed -i "s/memory_limit = .*/memory_limit = 2048M/" /usr/local/etc/php/php.ini
RUN cat /usr/local/etc/php/php.ini | grep memory_limit


